var webpack = require('webpack');
var path = require("path");

module.exports = {
    entry: path.join(__dirname, '/src/app-client.js'),
    output: {
        path: path.join(__dirname, '/src/static/js'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json']
    },
    devServer: {
        inline: true,
        contentBase: './src/static',
        port: 9000
    },
    module: {
        loaders: [{
            loader: 'babel-loader',
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            query:
            {
                presets:['es2015','react']
            }
        }]
    },
    target: 'web'
};
