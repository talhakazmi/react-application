const athletes = [
    {
        'id': 'player1',
        'name': 'Waseem Akram',
        'country': 'pk',
        'birth': '1966',
        'medals': [
            {
                'year': '1992', 'type': 'S', 'city': 'Barcelona', 'event': 'Cricket World Cup',
                'year': '1993', 'type': 'G', 'city': 'Berlin', 'event': 'Cricket T20 World Cup',
                'year': '1994', 'type': 'B', 'city': 'Karachi', 'event': 'Cricket Test World Cup'
            }
        ]
    },
    {
        'id': 'player2',
        'name': 'Imran Khan',
        'country': 'in',
        'birth': '1956',
        'medals': [
            {
                'year': '1992', 'type': 'G', 'city': 'Barcelona', 'event': 'Cricket World Cup',
                'year': '1993', 'type': 'S', 'city': 'Berlin', 'event': 'Cricket T20 World Cup',
                'year': '1994', 'type': 'B', 'city': 'Karachi', 'event': 'Cricket Test World Cup'
            }
        ]
    }
];

export default athletes;