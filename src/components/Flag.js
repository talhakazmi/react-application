import React from 'react';

const data = {
    'pk': {
        'name': 'Paksitan'
    },
    'in': {
        'name': 'India'
    }
};

export default class Flag extends React.Component {
    render() {
        const name = data[this.props.code].name;
        return (
            <span className="flag">
                {this.props.showName && <span className="name">{name}</span>}
            </span>
        );
    }
}