import React from 'react';
import { Link } from 'react-router';
import NotFoundPage from './NotFoundPage';
import AthletesMenu from './AthletesMenu';
import Medal from './Medal';
import Flag from './Flag';
import athletes from '../data/athletes';

export default class AthletePage extends React.Component {
    render() {
        const id = this.props.params.id;
        const athlete = athletes.filter((athlete) => athlete.id === id)[0];
        if(!athlete) {
            return <NotFoundPage />;
        }
        const headerStyle = { backgroundColor: `fefefe` };

        return (
            <div>
                <AthletesMenu athletes={athletes} />
                <div>
                    <header>
                        <div><h2>{athlete.name}</h2></div>
                        <section>
                            Cricketer born in <strong>{athlete.birth}, find out more on <a href={athlete.link}>here</a></strong>
                        </section>
                        <section>
                            <p>He Won <strong>{athlete.medals.length}</strong> medals:</p>
                            <ul>{
                                athlete.medals.map((medal, i) => <Medal key={i} {...medal} />)
                            }</ul>
                        </section>
                    </header>
                </div>
                <div>
                    <Link to="/">Back to index</Link>
                </div>
            </div>
        );
    }
}
