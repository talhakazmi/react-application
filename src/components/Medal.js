import React from 'react';

const typeMap = {
    'G': 'Gold',
    'S': 'Silver',
    'B': 'Bronz',
};

export default class Medal extends React.Component {
    render() {
        return (
            <li className="medal">
                <span title={typeMap[this.props.type]}>{this.props.type}</span>
                <span>{this.props.year}</span>
                <span>{this.props.city}</span>
                <span>{this.props.event}</span>
            </li>
        );
    }
}