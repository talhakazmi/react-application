import React from 'react';
import AthletePreview from './AthletePreview';
import athletes  from '../data/athletes';

export default class IndexPageComponent extends React.Component {
    render() {
        return (
            <div className="home">
                <div>
                    {athletes.map(athleteData => <AthletePreview key={athleteData.id} {...athleteData} />)}
                </div>
            </div>
        );
    }
}