import React from 'react';
import { Link } from 'react-router';

export default class Layout extends React.Component {
    render() {
        return (
            <div className="app-container">
                <header>
                    <Link to="/home">
                        Athletes List
                    </Link>
                </header>
                <div className="app-content">{this.props.children}</div>
                <footer>
                    <p>
                        This is a demo appication on React using WebPack. <Link to="/404">Check 404 Page</Link>
                    </p>
                </footer>
            </div>
        );
    }
}