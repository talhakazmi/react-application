import React from 'react';
import { Link } from 'react-router';

export default class AthletePreview extends React.Component {
    render() {
        return (
            <Link to={`/athlete/${this.props.id}`}>
                <div>
                    <div>{this.props.name}</div>
                    <div>{this.props.medals.length}</div>
                </div>
            </Link>
        );
    }
}