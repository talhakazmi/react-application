import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Layout from './components/Layout';
import AthletePage from './components/AthletePage';
import NotFoundPage from './components/NotFoundPage';
import  IndexPageComponent from './components/IndexPageComponent';

const routes = (
    <Route path="/" component={Layout}>
        <Route path="home" component={IndexPageComponent} />
        <Route path="athlete/:id" component={AthletePage} />
        <Route path="*" component={NotFoundPage} />
    </Route>
);


export default routes;